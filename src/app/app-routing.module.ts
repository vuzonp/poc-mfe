import { HelloWorldComponent } from './hello-world/hello-world.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: HelloWorldComponent,
    pathMatch: 'full'
  },
  {
    path: 'my-funnel',
    loadChildren: () => import('./my-funnel/my-funnel.module').then(m => m.MyFunnelModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    { enableTracing: true}
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
