import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyFunnelComponent } from './my-funnel.component';

describe('MyFunnelComponent', () => {
  let component: MyFunnelComponent;
  let fixture: ComponentFixture<MyFunnelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyFunnelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyFunnelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
