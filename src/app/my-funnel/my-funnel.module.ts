import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyFunnelRoutingModule } from './my-funnel-routing.module';
import { MyFunnelComponent } from './my-funnel.component';
import { Page2Component } from './page2/page2.component';


@NgModule({
  declarations: [
    MyFunnelComponent,
    Page2Component
  ],
  imports: [
    CommonModule,
    MyFunnelRoutingModule
  ]
})
export class MyFunnelModule { }
