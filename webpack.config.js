const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const mf = require("@angular-architects/module-federation/webpack");
const path = require("path");

const sharedMappings = new mf.SharedMappings();
sharedMappings.register(
  path.join(__dirname, 'tsconfig.json'),
  [/* mapped paths to share */]);

module.exports = {
  output: {
    uniqueName: "mfe1Poc",
    publicPath: "auto"
  },
  optimization: {
    runtimeChunk: false
  },
  resolve: {
    alias: {
      ...sharedMappings.getAliases(),
    }
  },
  plugins: [
    new ModuleFederationPlugin({

        // For remotes (please adjust)
        name: "mfe1Poc",
        filename: "remoteEntry.js",
        exposes: {
            // './Component': './/src/app/app.component.ts',
            './Module': './/src/app/my-funnel/my-funnel.module.ts'
        },

        // For hosts (please adjust)
        // remotes: {
        //     "mfe1": "mfe1@http://localhost:3000/remoteEntry.js",

        // },

        // see: https://www.angulararchitects.io/aktuelles/getting-out-of-version-mismatch-hell-with-module-federation/
        shared: {
          "@angular/core": {
            singleton: true,
            strictVersion: true,
            requiredVersion: ">=12.0.0 <13.0.0"
          },
          "@angular/common": {
            singleton: true,
            strictVersion: true,
            requiredVersion: ">=12.0.0 <13.0.0"
          },
          "@angular/common/http": {
            singleton: true,
            strictVersion: true,
            requiredVersion: ">=12.0.0 <13.0.0"
          },
          "@angular/router": {
            singleton: true,
            strictVersion: true,
            requiredVersion: ">=12.0.0 <13.0.0"
          },

          ...sharedMappings.getDescriptors()
        }

    }),
    sharedMappings.getPlugin()
  ],
};
